/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.oxgame;

/**
 *
 * @author ASUS
 */
import java.util.Scanner.*;
public class MainProgram {
      public static void main(String[] args) { 
        // Initialize the game-board 
        Table t = new Table();
        boolean gameStatus = false;             
         System.out.println("Welcome to OX Game");
         t.printTable();
        do {
            t.updateTable();
            t.printTable();
            // Print message if game over
            if (t.checkForWinner()) {
                System.out.println(">>>"+t.printPlayer()+"  Win<<<");
                gameStatus = true;
            }
            if (t.isTableFull()) {
                System.out.println(">>>Draw<<<");
                gameStatus = true;
            }
            // Switch player
            t.changePlayer();  
        } while (gameStatus == false); // repeat until game over.
        
        
    } 
      
}
